#pragma once
#include "USB_Types.hpp"
#include "PackedTuple.hpp"
#include <array>

namespace USB::HID
{
    #pragma pack(push,1)
    /**
     * @brief The avaliable country codes.
     */
    enum class CountryCodes : U<1>
    {
        NotSupported = 00, 
        Netherlands = 18,
        Dutch = 18,
        Arabic = 1, 
        Norwegian = 19,
        Belgian = 2, 
        Farsi = 20,
        CanadianBilingual = 3, 
        Poland = 21,
        CanadianFrench = 04, 
        Portuguese = 22,
        CzechRepublic = 5,  
        Russia = 23,
        Danish = 6, 
        Slovakia = 24,
        Finnish = 7, 
        Spanish = 25,
        French = 8, 
        Swedish = 26,
        German = 9, 
        SwissFrench = 27,
        Greek = 10, 
        SwissGerman = 28,
        Hebrew = 11,
        Switzerland = 29,
        Hungary = 12, 
        Taiwan = 30,
        International = 13,
        TurkishQ = 31,
        Italian = 14, 
        UK = 32,
        Japan = 15,
        US = 33,
        Korean = 16, 
        Yugoslavia = 34,
        LatinAmerican = 17,  
        TurkishF = 35,
    };
    /**
     * @brief This is the common header for all HID descriptors
     */
    struct DescriptorHeader
    {
        U<1>            bLength;
        DescriptorTypes bDescriptorType;
        Version         bcdHID;
        CountryCodes    bCountryCode;
        U<1>            bNumDescriptors;
    };
    /**
     * @brief Descriptor allocation
     */
    struct DescriptorAllocation
    {
        DescriptorTypes bDescriptorType;
        U<2>            bDescriptorLength;
    };
    /**
     * @brief A generic HID descriptor class that calculates automatically nessary size fields.
     * 
     * The class works via inheritance to allow empty base optimisation, critical if there are no descriptors that follow
     * Calling the USB GetData function is used to retrieve a number of bits.
     * Cast the recieved bits into Descriptor allocation if it is equal to the size of the descriptor allocation.
     * 
     * Args is the set of descriptors:
     * Valid descriptors are:
     * @li Report
     * @li Physical Descriptor
     */
    template <typename ... Allocation>
    struct Descriptor : 
        DescriptorHeader,
        PackedTuple<Allocation...>
    {
        /**
         * @brief Construct a hid descriptor. 
         * 
         * @param version The HID descriptor version.
         * @param country The country code of the decriptor.
         * @param allocation The component descriptors.
         */
        constexpr Descriptor( Version version, CountryCodes country, PackedTuple<Allocation...> allocation ) noexcept : 
            DescriptorHeader
            {
                sizeof( Descriptor ), 
                DescriptorTypes::HID, 
                version,
                country,
                sizeof...(Allocation)
            },
            PackedTuple<Allocation...>{ allocation }
        {}
    };
    /**
     * @brief A deduction guide for the HID descriptor
     * 
     * @tparam Allocation The component descriptors of the deduction guide.
     */
    template < typename ... Allocation >
    Descriptor( Version, CountryCodes, PackedTuple<Allocation...> ) -> Descriptor< Allocation ... >;
}