#pragma once
#include "Meta.hpp"
#include <cstdint>

namespace USB::PD
{
    enum class PortPowerRole : uint16_t
    {
        Sink = 0,
        Source = 1
    };
    enum class SpecificationRevision : uint16_t
    {
        Rev1_0 = 0b00,
        Rev2_0 = 0b01
    };
    enum class PortDataRole : uint16_t
    {
        UFP = 0,
        DFP = 1
    };
    enum class CablePlug : uint16_t 
    {
        OriginatedFromDFPorUFP      = 0u,
        OriginatedFromCablePlug     = 1u
    };
    enum class ControlMessageType : uint16_t 
    {
        GoodCRC                     = 0b0001u,
        GotoMin                     = 0b0010u,
        Accept                      = 0b0011u,
        Reject                      = 0b0100u,
        Ping                        = 0b0101u,
        PS_RDY                      = 0b0110u,
        Get_Source_Cap              = 0b0111u,
        Get_Sink_Cap                = 0b1000u,
        DR_Swap                     = 0b1001u,
        PR_Swap                     = 0b1010u,
        VCONN_Swap                  = 0b1011u,
        Wait                        = 0b1100u,
        Soft_Reset                  = 0b1101u,
    };
    enum class DataMessageType : uint16_t
    {
        Source_Capabilities         = 0b0001u,
        Request 	                = 0b0010u,
        BIST 	                    = 0b0011u,
        Sink_Capabilities           = 0b0100u,
        Vendor_Defined 	            = 0b1111u 
    };
    //
    //
    enum class TxFrameType : uint8_t
    {
        SOP                 = 0,
        SOPPrime            = 1,
        SOPPrimePrime       = 2,
        SOPDebugPrime       = 3,
        SOPDebugPrimePrime  = 4,
        HardReset           = 5,
        CableReset          = 6,
        BistMode_2          = 7
    };
    enum class TxRetryCnt : uint8_t
    {
        NoRetries   = 0,
        Once        = 1,
        Twice       = 2,
        Thrice      = 3
    };
    //
    //
    struct TxFrame
    {
        union
        {
            struct
            {
                TxFrameType TX_FRAME_TYPE   : 3;
                uint8_t     Reserved3       : 1;
                TxRetryCnt  TX_RETRY_CNT    : 2;
                uint8_t     Reserved6_7     : 2;
            } Fields;
            uint8_t Data;
        };
        //
        TxFrame( TxFrameType frame_type, TxRetryCnt tx_retry_cnt ) noexcept :
            Fields
            {
                frame_type,
                0,
                tx_retry_cnt,
                0
            }
        {}
    };
    //
    struct MessageIDGenerator
    {
        static inline uint32_t counter = 1; 
        operator uint32_t () const noexcept
        {
            return counter++;
        }
    };
    //
    //
    struct PDControlMessage
    {
        union
        {
            struct
            {
                ControlMessageType      bmMessageType           : 4;
                uint16_t                Reserved0               : 1;
                PortDataRole            bmPortDataRole          : 1;
                SpecificationRevision   bmSpecificationRevision : 2;
                PortPowerRole           bmPortPowerRole         : 1;
                uint16_t                bmMessageID             : 3;
                uint16_t                Zero                    : 3;
                uint16_t                Reserved1               : 1;
            } Fields;
            uint16_t Data;
        };
        //
        PDControlMessage(ControlMessageType message_type, PortDataRole port_data_role, SpecificationRevision spec_rev, PortPowerRole port_power_role) :
            Fields
            {
                message_type,
                0,
                port_data_role,
                spec_rev,
                port_power_role,
                MessageIDGenerator{},
                0,
                0
            }
        {}
    };
    
    struct PDDataMessageHeader
    {
        union
        {
            uint16_t Data;
            //
            struct 
            {
                DataMessageType         bmMessageType           : 4;
                uint16_t                Reserved0               : 1;
                PortDataRole            bmPortDataRole          : 1;
                SpecificationRevision   bmSpecificationRevision : 2;
                PortPowerRole           bmPortPowerRole         : 1;
                uint16_t                bmMessageID             : 3;
                uint16_t                bmNumberOfDataObjects   : 3;
                uint16_t                Reserved1               : 1;
            } Fields;
        };
    };
    //
    //
    template <size_t N>
    struct PDDataMessage : PDDataMessageHeader
    {
        uint32_t payload[N];
    };
    //
    //
    struct PDGenericMessageHeader
    {
        union
        {
            struct
            {
                uint16_t                bmMessageType           : 4;
                uint16_t                Reserved0               : 1;
                PortDataRole            bmPortDataRole          : 1;
                SpecificationRevision   bmSpecificationRevision : 2;
                PortPowerRole           bmPortPowerRole         : 1;
                uint16_t                bmMessageID             : 3;
                uint16_t                bmNumberOfDataObjects   : 3;
                uint16_t                Reserved1               : 1;
            } Fields;
            uint16_t Data;
        } Data;
        //
        PDGenericMessageHeader(uint16_t message_type, PortDataRole port_data_role, SpecificationRevision spec_rev, PortPowerRole port_power_role, uint8_t number_of_objects) :
            Data
            {
                {
                    message_type,
                    0,
                    port_data_role,
                    spec_rev,
                    port_power_role,
                    MessageIDGenerator{},
                    number_of_objects
                }
            }
        {}
        //
        // template <typename F>
        // void Transform(F && function) const noexcept
        // {
        //     // The message is a control message if there are zero data objects.
        //     if ( Data.Fields.bmNumberOfDataObjects == 0 )
        //     {
        //         std::forward<F>(function)( General::PunCast<PDControlMessage>( *this ) );
        //     }
        //     else
        //     {
        //         std::forward<F>(function)( General::PunCast<PDDataMessageHeader>( *this ) );
        //     }
        // }
    };
}