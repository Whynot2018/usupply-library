#pragma once
#include "Macros.hpp"
#include "USB_HW.hpp"
#include "USB_StandardDescriptors.hpp"
#include "USB_StandardDeviceRequest.hpp"
#include "USB_Configuration.hpp"
#include "USB_HIDInterface.hpp"
#include "Meta.hpp"
#include "GPIO.hpp"
#include "USB_Types.hpp"
#include <array>
#include <variant>

namespace USB
{
    /**
     * This initialises the usb module, configured with default endpoint 0.
     */
    using USB_DP            = System::Pins::USB_DP; 
    using USB_DN            = System::Pins::USB_DN;
    //
    struct USB_LL_Kernal
    {
        template <unsigned N>
        static constexpr bool UseEP = General::IsAnyOf(N, 0, 1);
    };
    using USB_LL            = Peripherals::USBModuleBase<USB_LL_Kernal, USB_DN, USB_DP>;
    using EP0               = Peripherals::Endpoint<0, Peripherals::EndpointDirection::IN_OUT,    64,     Peripherals::EndpointType::Control, Peripherals::ControlKind::Normal, Peripherals::Status::Stall, Peripherals::Status::Valid>;
    using EP1               = Peripherals::Endpoint<1, Peripherals::EndpointDirection::IN,        64,     Peripherals::EndpointType::Interrupt, 0, Peripherals::Status::NAK, Peripherals::Status::Disabled>;
    /**
     * 
     */
    using USBActionCallback = Peripherals::USBActionCallback;
    using USBAction         = Peripherals::USBAction;
    using USBEvent          = Peripherals::USBEvent;
    using USBReceiveType    = Peripherals::USBReceiveType;
    using ReceiveData       = Peripherals::ReceiveData_t;
    using TransmitData      = Peripherals::TransmitData_t;
    struct TransmitComplete{};
    /**
     * @brief 
     * 
     */
    template <typename T>
    auto HasKeepAlive(...) -> std::false_type { return {}; }
    //
    template <typename T>
    auto HasKeepAlive(int) -> decltype( ( std::declval<T>().KeepAlive( std::declval<USBActionCallback>() ), std::true_type{} ) ) { return {}; }
    //
    template <typename T>
    constexpr bool HasKeepAlive_v = decltype(HasKeepAlive<std::remove_reference_t<T>>(0)){};
    /**
     * 
     */
    template <typename Base>
    struct EventCallback;
    /**
     * 
     */
    template <template <typename Type> class Base, typename ... Interfaces>
    struct EventCallback< Base< Interfaces ... > >
    {
        using base_t = Base< Interfaces ... >;
        /**
         * 
         */
        void operator ()( uint8_t endpoint, USBActionCallback action, USBEvent e ) const noexcept
        {
            using namespace ::System;
            switch ( e )
            {
            case USBEvent::StatusComplete:
            {
                bool handled =
                (
                    ( endpoint == 0 ) and base_t::StatusComplete( endpoint, action ) 
                )
                or
                (
                    ( [&]() -> bool
                    {
                        for ( auto ep : Interfaces::Endpoints )
                        {
                            if ( endpoint == ep )
                            {
                                return Interfaces::StatusComplete( endpoint, action );
                            }
                        }
                        return false;
                    }() )
                    or
                    ...
                );
                //
                if ( !handled )
                {
                }
                break;
            }
            case USBEvent::EndpointSetup:
            {
                /**
                 * Endpoint 0 is a Control pipe always present in USB devices. Therefore,
                 * only the Interrupt In pipe is described for the Interface descriptor using an
                 * Endpoint descriptor. In fact, several Interface descriptors may share Endpoint 0.
                 * An Interrupt Out pipe is optional and requires an additional Endpoint descriptor
                 * if declared.
                 */
                USB_LL::ConfigureEndpoints
                (
                    EP0{},
                    EP1{}
                );
                break;
            }
            case USBEvent::SuspendEvent:
            {
                System::pwr_en = false;
                break;
            }
            case USBEvent::WakeupEvent:
            {
                System::pwr_en = true;
                break;
            }
            case USBEvent::TransmitComplete:
            {
                bool handled = ( [&]() -> bool
                {
                    for ( auto ep : Interfaces::Endpoints )
                    {
                        if ( endpoint == ep )
                        {
                            return Interfaces::TransmitComplete(endpoint, action);
                        }
                    }
                    return false;
                }() or ... );
                //
                if ( !handled )
                {
                }
                break;
            }
            case USBEvent::StartOfFrame:
            {
                ( [&]() -> bool
                {
                    if constexpr ( HasKeepAlive_v<Interfaces> )
                    {
                        return Interfaces::KeepAlive( action );
                    }
                    return false;
                }() or ... );
                break;
            }
            default:
                break;
            }
        }
    };
    //
    //
    template <typename Base>
    struct SetupOutCallback;
    //
    //
    template <template <typename ...> class Base, typename ... Interfaces>
    struct SetupOutCallback< Base< Interfaces ... > >
    {
        using base_t = Base< Interfaces ... >;
        //
        //
        void operator() ( uint8_t endpoint, USBActionCallback action, USBReceiveType type, Containers::DataView<uint8_t> d ) const noexcept
        {
            switch ( type )
            {
                case USBReceiveType::OUT:
                {
                    bool handled = 
                    (
                        [&]() -> bool
                        {
                            for ( auto ep : Interfaces::Endpoints )
                            {
                                if ( endpoint == ep )
                                {
                                    return Interfaces::RecieveComplete( endpoint, action, d );
                                }
                            }
                            return false;
                        }()
                        or
                        ...
                    );
                    /**
                     * Unhandled transactions get trapped here
                     */
                    if ( !handled )
                    {
                    }
                    break;
                }
                case USBReceiveType::SETUP:
                {
                    /**
                     * Handle the transaction
                     */
                    if ( d.Size() >= sizeof( StandardDeviceRequest ) )
                    {
                        /**
                         * Convert the data to its specific request
                         */
                        auto [ setup, data ] = d.Split( sizeof( StandardDeviceRequest ) );
                        StandardDeviceRequest request = ( StandardDeviceRequest const & ) * setup.Data();
                        /**
                         * The above flag is assigned only if one of the transformations handles this event.
                         */
                        bool handled = 
                        (
                            /**
                             * uint8_t endpoint, StandardDeviceRequest request, F && result_callback
                             */
                            Transform
                            (
                                endpoint,
                                request,
                                [action]( uint8_t ep, auto packet ) noexcept
                                {
                                    /**
                                     * bRequest (a member of packet) stores the type.
                                     * Dispatch to typed overload functions this prevents
                                     * double handling with the bRequest variable and 
                                     * provides type safety.
                                     */
                                    base_t::Request( ep, action, packet );
                                }
                            )
                            or
                            (
                                Interfaces::Transform( endpoint, action, request )
                                or
                                ...
                            )
                        );
                        /**
                         * Unhandled transactions get trapped here
                         */
                        if ( !handled )
                        {
                        }
                    }
                    else
                    {
                    }
                    break;
                }
            };
        }
    };
    //
    //
    template <typename ... Interfaces>
    struct USBManager : Peripherals::USBModule<USB_LL, EventCallback<USBManager<Interfaces...>>, SetupOutCallback<USBManager<Interfaces...>>>
    {
        /**
         * 
         */
        using base_t = Peripherals::USBModule<USB_LL, EventCallback<USBManager<Interfaces...>>, SetupOutCallback<USBManager<Interfaces...>>>;
        /**
         * 
         */
        alignas(2) static constexpr StandardDeviceDescriptor DeviceDescriptor
        {
            {},
            Version{ 2 },   // USB Spec Release Number in BCD format
            0x00,           // Class Code
            0x00,           // Subclass code
            0x00,           // Protocol code
            64,             // Max packet size for EP0, see usb_config.h
            0x04D8,         // Vendor ID
            0x0009,         // Product ID: Microchip Mouse Demo PID
            Version{ 1 },   // Device release number
            UniqueID(),     // Manufacturer string index
            UniqueID(),     // Product string index
            UniqueID(),     // Device serial number string index
            0x01            // Number of possible configurations
        };
        alignas(2) static constexpr PackagedConfiguration ConfigurationDescriptor
        {
            AutoConfiguration
            {
                0u,
                UniqueID(),
                BusPowered,
                MaxPower{}
            },
            PackedTuple
            {
                Interfaces::Descriptor...
            }
        };
        alignas(2) static constexpr DeviceQualifierDescriptor QualifierDescriptor
        {
            {},
            Version{ 2 },
            0x00,
            0x00,
            0x00,
            MaxPacketSize{ 64 },
            0x01,
            0x00
        };
        /**
         * Standard string descriptors
         */
        alignas(2) static constexpr StringDescriptorZero   language        { LanguageID::Australia };
        /**
         * @brief Manufacturer string descriptor.
         */
        alignas(2) static constexpr StringDescriptor       manufacturer    { "Jonestronics Pty Ltd" };
        /**
         * @brief Product string descriptor.
         */
        alignas(2) static constexpr StringDescriptor       product         { "uSupply" };
        /**
         * @brief The serial number string descriptor.
         */
        alignas(2) static constexpr StringDescriptor       serial_number   { "USUP00001" };
        /**
         * @brief The configuration descriptor.
         */
        alignas(2) static constexpr StringDescriptor       configuration   { "uSupply Normal Power Mode" };
        /**
         * @brief Pending is used ONLY from within interrupts. 
         * 
         * Because pending is only accessed from within interrupts the 
         * the control flow can be understood by the compiler (there is only 1 control flow)
         * for this reason volatile doesn't need to be volatile.
         */
        inline static unsigned Pending = false;
        /**
         * @brief Handles the get descriptor request.
         * 
         * @param endpoint The endpoint that the request was from.
         * @param action An abstraction that allows access to hardware with the correct endpoint settings.
         * @param value The descriptor.
         */
        static ALWAYS_INLINE void Request( uint8_t endpoint, USBActionCallback action, GetDescriptor value )
        {
            auto process = [&] ( auto & desc )
            {
                Pending = true;
                action( { USBAction::TransmitRaw, RBytes( desc, value.wLength ) } );
            };
            /**
             * Transmits on the endpoint
             */
            switch (value.wValue.DescriptorType)
            {
            case DescriptorTypes::DEVICE:
            {
                process( DeviceDescriptor );
                break;
            }
            case DescriptorTypes::STRING:
            {
                auto v = value.wValue.DescriptorIndex;
                if      (v == 0)
                    process( language );
                else if (v == DeviceDescriptor.iProduct)
                    process( product );
                else if (v == DeviceDescriptor.iManufacturer)
                    process( manufacturer );
                else if (v == DeviceDescriptor.iSerialNumber)
                    process( serial_number );
                else if (v == ConfigurationDescriptor.iConfiguration)
                    process( configuration );
                else
                {
                    ([&](auto index) -> bool
                    {
                        if ( index == Interfaces::StringIndex )
                        {
                            process( Interfaces::String );
                            return true;
                        }
                        return false;
                    }(v) or ... );
                }
                break;
            }
            case DescriptorTypes::DEVICE_QUALIFIER:
            {
                process( QualifierDescriptor );
                break;
            }
            case DescriptorTypes::OTHER_SPEED_CONFIGURATION:
            case DescriptorTypes::CONFIGURATION:
            {
                process( ConfigurationDescriptor );
                break;
            }
            default:
                break;
            }
        }
        /**
         * @brief Handles the set address request.
         * 
         * @param endpoint The endpoint that the request was from.
         * @param action An abstraction that allows access to hardware with the correct endpoint settings.
         * @param value The descriptor.
         */
        static ALWAYS_INLINE void Request( uint8_t endpoint, USBActionCallback action, SetAddress value )
        {
            Pending = true;
            action( { USBAction::SetAddress, { value.wValue.DeviceAddress } });
        }
        /**
         * @brief Handles the set configuration request.
         * 
         * @param endpoint The endpoint that the request was from.
         * @param action An abstraction that allows access to hardware with the correct endpoint settings.
         * @param value The descriptor.
         */
        static ALWAYS_INLINE void Request( uint8_t endpoint, USBActionCallback action, SetConfiguration value )
        {
            Pending = true;
            action( { USBAction::Status } );
        }
        /**
         * @brief Handles the get configuration request.
         * 
         * @param endpoint The endpoint that the request was from.
         * @param action An abstraction that allows access to hardware with the correct endpoint settings.
         * @param value The descriptor.
         */
        static ALWAYS_INLINE void Request( uint8_t endpoint, USBActionCallback action, GetConfiguration value )
        {
            uint8_t data = 0x00;
            Pending = true;
            action( { USBAction::TransmitRaw, RBytes( data, value.wLength ) } );
        }
        /**
         * @brief Handles the clear feature request.
         * 
         * @param endpoint The endpoint that the request was from.
         * @param action An abstraction that allows access to hardware with the correct endpoint settings.
         * @param value The descriptor.
         */
        static ALWAYS_INLINE void Request( uint8_t endpoint, USBActionCallback action, ClearFeature value )
        {
            switch ( value.bmRequestType.Recipient )
            {
                case RequestRecipient::Device:
                {
                    switch ( value.wValue )
                    {
                    case FeatureSelector::DEVICE_REMOTE_WAKEUP:
                        break;    
                    case FeatureSelector::TEST_MODE:
                        break;
                    default:
                        break;
                    }
                    break;
                }
                case RequestRecipient::Endpoint:
                {
                    if ( value.wValue == FeatureSelector::ENDPOINT_HALT )
                    {
                    }
                    break;
                }
                default:
                    break;
            }
        }
        /**
         * @brief Handles the set feature request.
         * 
         * @param endpoint The endpoint that the request was from.
         * @param action An abstraction that allows access to hardware with the correct endpoint settings.
         * @param value The descriptor.
         */
        static ALWAYS_INLINE void Request( uint8_t endpoint, USBActionCallback action, SetFeature value )
        {
            switch ( value.bmRequestType.Recipient )
            {
                case RequestRecipient::Device:
                {
                    switch ( value.wValue )
                    {
                    case FeatureSelector::DEVICE_REMOTE_WAKEUP: 
                        break;    
                    case FeatureSelector::TEST_MODE: 
                        break;
                    default:
                        break;
                    }
                    break;
                }
                case RequestRecipient::Endpoint:
                {
                    if (value.wValue == FeatureSelector::ENDPOINT_HALT)
                    {
                    }
                    break;
                }
                case RequestRecipient::Interface:
                {
                    break;
                }
                default: break;
            }
        }
        /**
         * @brief Handles the get interface request.
         * 
         * @param endpoint The endpoint that the request was from.
         * @param action An abstraction that allows access to hardware with the correct endpoint settings.
         * @param value The descriptor.
         */
        static ALWAYS_INLINE void Request( uint8_t endpoint, USBActionCallback action, GetInterface value )
        {
            (
                ( [ action, value, endpoint ]() -> bool
                {
                    if ( value.wIndex.Interface == Interfaces::Number )
                    {
                        Interfaces::Request( endpoint, action, value );
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }() ) 
                or
                ...
            );
        }
        /**
         * @brief Handles the set interface request.
         * 
         * @param endpoint The endpoint that the request was from.
         * @param action An abstraction that allows access to hardware with the correct endpoint settings.
         * @param value The descriptor.
         */
        static ALWAYS_INLINE void Request( uint8_t endpoint, USBActionCallback action, SetInterface value )
        {
            (
                ( [ action, value, endpoint ]() -> bool
                {
                    if ( value.wIndex.Interface == Interfaces::Number )
                    {
                        Interfaces::Request( endpoint, action, value );
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }() )
                or
                ...
            );
        }
        /**
         * @brief Handles the get status request.
         * 
         * @param endpoint The endpoint that the request was from.
         * @param action An abstraction that allows access to hardware with the correct endpoint settings.
         * @param value The descriptor.
         */
        static ALWAYS_INLINE void Request( uint8_t endpoint, USBActionCallback action, GetStatus value )
        {
            switch ( value.bmRequestType.Recipient )
            {
                case RequestRecipient::Device:
                {
                    DeviceStatus desc
                    {
                        false,  // Not self powered (its bus powered)
                        false   // No remote wake-up
                    };
                    //
                    Pending = true;
                    action( { USBAction::TransmitRaw, RBytes( desc, value.wLength ) } );
                    break;
                }
                case RequestRecipient::Interface:
                {
                    (
                        ( [action, value, endpoint]() -> bool
                        {
                            if ( value.wIndex == Interfaces::Number )
                            {
                                Interfaces::Request( endpoint, action, value );
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }() )
                        or
                        ...
                    );
                    break;
                }
                case RequestRecipient::Endpoint:
                {
                    Pending = true;
                    EndpointStatus desc{ 0, 0 };
                    action( { USBAction::TransmitRaw, RBytes( desc, value.wLength ) } );
                    break;
                }
                default:
                {
                    break;
                }
            }
        }
        /**
         * @brief Handles the set descriptor request.
         * 
         * @warning This does nothing.
         * 
         * @param endpoint The endpoint that the request was from.
         * @param action An abstraction that allows access to hardware with the correct endpoint settings.
         * @param value The descriptor.
         */
        static ALWAYS_INLINE void Request( uint8_t endpoint, USBActionCallback action, SetDescriptor value ){}
        /**
         * @brief Handles the synch frame request.
         * 
         * @warning This does nothing.
         * 
         * @param endpoint The endpoint that the request was from.
         * @param action An abstraction that allows access to hardware with the correct endpoint settings.
         * @param value The descriptor.
         */
        static ALWAYS_INLINE void Request( uint8_t endpoint, USBActionCallback action, SynchFrame value )   {}
        /**
         * @brief Called when the status stage was completed from a pending transaction.
         * 
         * @param endpoint The endpoint that the request was from.
         * @param action An abstraction that allows access to hardware with the correct endpoint settings.
         */
        static ALWAYS_INLINE bool StatusComplete( uint8_t endpoint, USBActionCallback action )
        {
            if ( Pending )
            {
                action( { USBAction::ReceiveValid } );
                Pending = false;
                return true;
            }
            else
            {
                return false;
            }
        }
        /**
         * @brief This initalises the USB peripheral with the above Standard Device Request callbacks
         */
        USBManager() noexcept :
            base_t
            {
                General::Type   < USB_LL     >{},
                EventCallback   < USBManager >{},
                SetupOutCallback< USBManager >{}
            }
        {}
    };
}