#pragma once
#include "CriticalSection.hpp"
#include "DataView.hpp"
#include "Meta.hpp"
#include "Version.hpp"
#include "PackedTuple.hpp"
#include "Integer.hpp"

namespace USB
{
    #pragma pack(push,1)
    /**
     * @brief Sets up a USB specific critcal section.
     * 
     * This data structure manages the disabling and restoring of interrupts for a USB specific critical section.
     */
    using CriticalSection = Peripherals::CriticalSection;
    /**
     * @brief A dataview used for readonly data.
     * 
     */
	using TransmitData 	= Containers::DataView<uint8_t, true>;
    /**
     * @brief A dataview used for writable data.
     * 
     */
	using ReceiveData 	= Containers::DataView<std::uint8_t, false>;
    /**
     * @brief A metaclass used to represent the lack of data. 
     */
    struct Nothing{};
    /**
     * @brief Test whether a type is Nothing.
     * 
     * @tparam T The type to test.
     */
    template <typename T>
    inline constexpr bool IsNothing_v = false;
    template <>
    inline constexpr bool IsNothing_v<Nothing> = true;
    /**
     * 
     */
    #pragma pack(push, 1)
    using Version = General::Version;
    /**
     * @brief A helper class used to hasten the development of descriptors 
     * allows pasting from the specification
     */
    template <size_t N>
    struct UINTBYTES{};
    template<>
    struct UINTBYTES<1> : General::Type<uint8_t> {};
    template<>
    struct UINTBYTES<2> : General::Type<uint16_t> {};
    template<>
    struct UINTBYTES<4> : General::Type<uint32_t> {};
    /**
     * @brief An alias for the UINTBYTES<N> helper class.
     */
    template <size_t N>
    using U = typename UINTBYTES<N>::type;
    /**
     * @brief Metaprogramming tool, picks the larger of two types.
     */
    template <typename A, typename B>
    using PickLarger_t = std::conditional_t<(sizeof(A) > sizeof(B)), A, B>;
    /**
     * 
     */
    template <typename T, auto value, typename O = PickLarger_t<T, decltype(value)>>
    constexpr bool IsUnchanged_v = (O)value <= (O)std::numeric_limits<T>::max();
    /**
     * @brief Gets the minimum size unsigned integer which can hold a value.
     */
    template <auto V>
    using MinSizeU = std::conditional_t
    <
        IsUnchanged_v<U<1>, V>,
        U<1>,
        std::conditional_t
        <
            IsUnchanged_v<U<2>, V>,
            U<2>,
            std::conditional_t
            <
                IsUnchanged_v<U<4>, V>,
                U<4>,
                void
            >
        >
    >;
    /**
     * @brief This serialises anything with POD.
     * 
     * @warning Technically this is undefined behaviour. Prefer PunCast for future code.
     * 
     * @tparam T The type to convert from.
     * @param object The data to serialise.
     * @param object_size The number of bytes in the object.
     * @return Containers::DataView<uint8_t> A view into the bytes in the object.
     */
    template <typename T>
    Containers::DataView<uint8_t> Bytes(T const & object, std::size_t object_size = sizeof(std::decay_t<T>)) noexcept
    {
        return { (uint8_t const *)&object, std::min(object_size, sizeof(std::decay_t<T>)) };
    }
    /**
     * @brief This serialises anything with POD.
     * 
     * This returns readonly view.
     * 
     * @warning Technically this is undefined behaviour. Prefer PunCast for future code.
     * 
     * @tparam T The type to convert from.
     * @param object The data to serialise.
     * @param object_size The number of bytes in the object.
     * @return Containers::DataView<uint8_t, true> A view into the bytes in the object.
     */
    template <typename T>
    Containers::DataView<uint8_t, true> RBytes( T const & object, size_t object_size = sizeof(std::decay_t<T>) ) noexcept
    {
        return { (uint8_t const *)&object, std::min( object_size, sizeof( std::decay_t<T> ) ) };
    }
    /**
     * @brief This serialises anything with POD.
     * 
     * This returns writable view.
     * 
     * @warning Technically this is undefined behaviour. Prefer PunCast for future code.
     * 
     * @tparam T The type to convert from.
     * @param object The data to serialise.
     * @param object_size The number of bytes in the object.
     * @return Containers::DataView<uint8_t, true> A view into the bytes in the object.
     */
    template <typename T>
    Containers::DataView<uint8_t, false> WBytes( T & object, size_t object_size = sizeof(std::decay_t<T>) ) noexcept
    {
        return { (uint8_t *)&object, std::min( object_size, sizeof( std::decay_t<T> ) ) };
    }
    /**
     * @brief The possible types of descritor supported by the usb library.
     */
    enum class DescriptorTypes : U<1>
    {
        DEVICE                      = 1,
        CONFIGURATION               = 2,
        STRING                      = 3,
        INTERFACE                   = 4,
        ENDPOINT                    = 5,
        DEVICE_QUALIFIER            = 6,
        OTHER_SPEED_CONFIGURATION   = 7,
        INTERFACE_POWER             = 8,
        /**
         * HID Specific Codes
         */
        HID = 0x21,
        HID_Report = 0x22,
        HID_PhysicalDescriptor = 0x23
    };
    /**
     */
    enum class Direction : U<1>
    {
        IN = 1,
        OUT = 0
    };
    /**
     * @brief The different request codes in a descriptor.
     */
    enum class RequestCodes : std::uint8_t 
    {   
        GET_STATUS          = 0,
        CLEAR_FEATURE       = 1,
        RESERVED0           = 2,
        SET_FEATURE         = 3,
        RESERVED1           = 4,
        SET_ADDRESS         = 5,
        GET_DESCRIPTOR      = 6,
        SET_DESCRIPTOR      = 7,
        GET_CONFIGURATION   = 8,
        SET_CONFIGURATION   = 9,
        GET_INTERFACE       = 10,
        SET_INTERFACE       = 11,
        SYNCH_FRAME         = 12,
        /**
         * HID Specific Codes
         */
        HID_GET_REPORT      = 0x01,
        HID_GET_IDLE        = 0x02,
        HID_GET_PROTOCOL    = 0x03,
        HID_SET_REPORT      = 0x09,
        HID_SET_IDLE        = 0x0A,
        HID_SET_PROTOCOL    = 0x0B
    };
    /**
     * This is a enumerated bitfield class
     */
    enum class RequestRecipient : U<1>
    {
        Device      = 0,
        Interface   = 1,
        Endpoint    = 2,
        Other       = 3
    };
    /**
     */
    enum class RequestDirection : U<1>
    {
        HostToDevice = 0,
        DeviceToHost = 1
    };
    /**
     * @brief A class that stores the request type of a class.
     * 
     */
    struct RequestType
    {
        RequestRecipient Recipient : 5u;
        /**
         * @note This field is called Type, but it is a request type
         * This results in a name conflict, to avoid this, the
         * enum is defined within the class
         */
        enum : U<1>
        {
            Standard = 0,
            Class = 1,
            Vendor = 2,
            Reserved = 3
        }
        Type : 2u;
        /**
         */
        RequestDirection Direction : 1u;
    };
    static_assert(sizeof(RequestType) == 1, "The size of the RequestType const ");
    /**
     * @brief Represents the maximum packet size as its own type. 
     */
    enum class MaxPacketSize : uint8_t {};
    /**
     * @brief Stores the maximum power for a usb descriptor. 
     */
    struct MaxPower
    {
        U<1> m_Power = 0xFF;
        //
        constexpr MaxPower() = default;
        /**
         * @brief Constructs a new MaxPower class.
         * 
         * @param power The power in mA.
         */
        constexpr MaxPower( unsigned power ) noexcept : 
            m_Power{ [](unsigned value) -> U<1> { return static_cast<U<1>>(value / 2u); }(power) }
        {}
    };
    /**
     * @brief A common header for many descriptors.
     * 
     * Used by inheriting from this class.
     */
    template <typename T, DescriptorTypes type>
    struct StandardHeader
    {
        U<1> const              bLength             = sizeof(T);
        DescriptorTypes const   bDescriptorType     = type;
    };
    /**
     * @brief The Setup Packet
     *  This is the base definition for the setup packet
     */
    struct StandardDeviceRequest
    {
        RequestType const   bmRequestType;
        /**
         * 
         */
        RequestCodes const bRequest;
        /**
         * 
         */
        std::uint16_t wValue;
        std::uint16_t wIndex;
        std::uint16_t wLength;
    };
	/**
	 * @brief Build a byte array from the input parameters
	 */
	template <typename ... Args>
	constexpr auto ByteArray(Args ... args) noexcept -> std::array<U<1>, sizeof...(Args)>
	{
		return { (U<1>)args... };
	}
    #pragma pack(pop)
}