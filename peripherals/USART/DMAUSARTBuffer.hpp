#pragma once
#include "DataView.hpp"

namespace Peripherals
{
    template <size_t N>
    class CircularBuffer
    {
        using View = Containers::DataView<uint8_t>;
        size_t previous = 0;
        size_t previous_remaining = 0;
    public:
        /**
         * @brief Resets the state of the data structure;
         * 
         */
        void Reset(size_t remaining) noexcept { previous = 0; previous_remaining = remaining; }
        /**
         * @brief A functor that runs a callback with the slices that have been processed.
         * 
         * This is useful with a DMA, if a DMA has only partially processed its data.
         * 
         * @tparam Data A view of the buffer that the DMA operates on.
         * @tparam C A callback which is called with the slices that the DMA has completed processing.
         * @param remaining The number of items the DMA still has to send.
         * @param d See Data.
         * @param callback See C.
         */
        template <typename Data, typename C>
        void operator()(size_t remaining, Data const & d, C && callback) noexcept
        {
            auto count = previous_remaining - remaining;
            previous_remaining = remaining;
            //
            if (not count)
            {
                return;
            }
            //
            auto current = (previous + count) % N;
            //
            auto c{ std::forward<C>(callback) };
            if (current > previous)
            {
                size_t len = (current - previous) % ( N + 1 );
                c( View{ &d[previous], len } );
            }
            else
            {
                size_t rhs_len = N - previous;
                size_t lhs_len = current;
                //
                c( View{ &d[previous], rhs_len } );
                c( View{ &d[0],		   lhs_len } );
            }
            //
            previous = current;
        }
    };
}