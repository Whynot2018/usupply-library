#pragma once

#include "PinDefinitions.hpp"
#include "Constants.hpp"
#include "Power.hpp"
#include "Pin.hpp"
#include "Interrupt.hpp"
#include "AtIndex.hpp"

#include <cstdint>
#include <array>

namespace Peripherals
{
	namespace DACGeneral
	{
		using Pins_t = General::TypeList<Pins::DAC_OUT1, Pins::DAC_OUT1>;

		template <unsigned count>
		using array_t = std::array<std::uint16_t, count>;
		/**
		 * @brief A helper class that 
		 * 
		 * @tparam Base the base class for the DAC. 
		 * @tparam Channel The DAC channels to initialise.
		 */
		template<typename Base, std::uint32_t  ... Channel>
		struct ChannelHelper;

		template<typename Base,  std::uint32_t CH_1, std::uint32_t  CH_2>
		struct ChannelHelper<Base, CH_1, CH_2>
		{
			using type = typename Base::type;
			void operator ()( type const value_1, type const value_2 ) noexcept
			{
				ChannelHelper<Base, CH_1> c1;
				ChannelHelper<Base, CH_2> c2;

				c1( value_1 );
				c2( value_2 );
			}
		};
		/**
		 * @brief Initialises a single DAC channel.
		 * 
		 * @tparam Base The DAC class.
		 * @tparam CH The channel to initialise.
		 */
		template<typename Base, std::uint32_t CH>
		struct ChannelHelper<Base, CH>
		{
			using type = typename Base::type;
			void operator ()( type const value ) noexcept
			{
				static_assert( CH == 1u || CH == 2u, "DAC only has two writtable channels." );
				if constexpr ( CH == 1u )
					Base::Channel1( value );
				if constexpr ( CH == 2u )
					Base::Channel2( value );
			}
		};
		/**
		 * @brief A helper class to enable the numbered DAC channel.
		 * 
		 * This class is used to inject common functionality into all of the above ChannelHelper classes.
		 */
		struct EnabledHelper
		{
			using type = bool;
			/**
			 * @brief Enables or disables the DAC1 channel.
			 * 
			 * @param value Whether or not to enable the channel.
			 */
			static void Channel1(type value) noexcept
			{
				DAC->CR |= 3u << 19u;
				auto & cr = DAC->CR;
				if (value)  cr 	|=  DAC_CR_EN1;
				else 		cr 	&= ~DAC_CR_EN1;
			}
			/**
			 * @brief Enables or disables the DAC2 channel.
			 * 
			 * @param value Whether or not to enable the channel.
			 */
			static void Channel2(type value) noexcept
			{
				DAC->CR |= 3u << 19u;
				auto & cr = DAC->CR;
				if (value)  cr 	|=  DAC_CR_EN2;
				else 		cr 	&= ~DAC_CR_EN2;
			}
		};
		/**
		 * @brief Enables two channels, given the two channel indexes.
		 * 
		 * @tparam CH_1 The first channel index.
		 * @tparam CH_2 The second channel index.
		 * @param enable_1 Enables the channel at the first channel index.
		 * @param enable_2 Enables the channel at the second channel index.
		 */
		template <unsigned CH_1, unsigned CH_2>
		void Enabled( bool enable_1, bool enable_2 ) noexcept
		{
			ChannelHelper<EnabledHelper, CH_1, CH_2> writter;
			writter( enable_1, enable_2 );
		}
		/**
		 * @brief Enable or disables both DAC channels.
		 * 
		 * @tparam CH_1 The first channel index to enable.
		 * @tparam CH_2 The second channel index to enable.
		 * @param enable Whether to enable or disable both channels.
		 */
		template <unsigned CH_1, unsigned CH_2>
		void Enabled( bool enable )
		{
			ChannelHelper<EnabledHelper, CH_1, CH_2> writter;
			writter(enable, enable);
		}
		/**
		 * @brief Enable or disable a single DAC channel.
		 * 
		 * @tparam CH The channel to enable or disable.
		 * @param enable Whether to enable or disable the channel.
		 */
		template <unsigned CH>
		void Enabled( bool enable )
		{
			ChannelHelper<EnabledHelper, CH> writter;
			writter( enable );
		}
		/**
		 * @brief A base class for the enabling or disabling of the DAC buffer at a numbered channel.
		 */
		struct BufferHelper
		{
			using type = bool;
			/**
			 * @brief Enables or disables the DAC buffer for DAC1 channel.
			 * 
			 * @param value Whether or not to enable buffer.
			 */
			static void Channel1( type value ) noexcept
			{
				auto & cr = DAC->CR;
				if (!value) 	cr 	|=  DAC_CR_BOFF1;
				else 			cr 	&= ~DAC_CR_BOFF1;
			}
			/**
			 * @brief Enables or disables the DAC buffer for DAC2 channel.
			 * 
			 * @param value Whether or not to enable buffer.
			 */
			static void Channel2( type value ) noexcept
			{
				auto & cr = DAC->CR;
				if (!value) 	cr 	|=  DAC_CR_BOFF2;
				else 			cr 	&= ~DAC_CR_BOFF2;
			}
		};
		/**
		 * @brief 
		 * 
		 * @tparam CH_1 
		 * @tparam CH_2 
		 * @param enable_1 
		 * @param enable_2 
		 */
		template <unsigned CH_1, unsigned CH_2>
		void Buffered( bool enable_1, bool enable_2 ) noexcept
		{
			ChannelHelper<BufferHelper, CH_1, CH_2> writter;
			writter( enable_1, enable_2 );
		}
		/**
		 * @brief Enables or disables two channel buffers. 
		 * 
		 * @tparam CH_1 The index of the channel.
		 * @tparam CH_2 The index of the channel.
		 * @param enable Whether or not to enable (true) or disable (false) both DAC buffers.
		 */
		template <unsigned CH_1, unsigned CH_2>
		void Buffered( bool enable ) noexcept
		{
			ChannelHelper<BufferHelper, CH_1, CH_2> writter;
			writter( enable, enable );
		}
		/**
		 * @brief Enables channel buffer. 
		 * 
		 * @tparam CH The index of the channel.
		 * @param enable Whether or not to enable (true) or disable (false) both DAC buffers.
		 */
		template <unsigned CH>
		void Buffered( bool enable ) noexcept
		{
			ChannelHelper<BufferHelper, CH> writter;
			writter( enable );
		}
		/**
		 * @brief An enum which is used to setup the trigger source for the DAC.
		 */
		enum class TriggeredFrom
		{
			Timer6 					=	0b000,
			Timer3 					=	0b001,
			Timer7 					=	0b010,
			Timer15					=	0b011,
			Timer2 					=	0b100,
			Reserved 				=	0b101,
			ExternalInteruptLine9	=	0b110,
			Software 				=	0b111,
			Disabled
		};
		/**
		 * @brief A class used as a helper to enable or disable an DAC channel trigger.
		 */
		struct TriggeredHelper
		{
			using type = TriggeredFrom;
			using cast_t = std::uint32_t;
			/**
			 * @brief Enables DAC1 trigger.
			 * 
			 * @param value Whether or not to enable the channel trigger.
			 */
			static void Channel1( type value ) noexcept
			{
				auto & cr = DAC->CR;
				cast_t from_v = (cast_t)value;

				if ( value != type::Disabled )
					cr 	|=  DAC_CR_TEN1;
				else
					cr 	&= ~DAC_CR_TEN1;

				cr |= from_v << 3u;
			}
			/**
			 * @brief Enables DAC2 trigger.
			 * 
			 * @param value Whether or not to enable the channel trigger.
			 */
			static void Channel2(type const value) noexcept
			{
				auto & cr = DAC->CR;
				cast_t from_v = (cast_t)value;

				if ( value != type::Disabled )
					cr	|=  DAC_CR_TEN2;
				else
					cr 	&= ~DAC_CR_TEN2;

				cr |= from_v << 19u;
			}
		};
		/**
		 * @brief Select the trigger source for a DAC channel.
		 * 
		 * @tparam CH The channel to select the trigger source.
		 * @param from The trigger source.
		 */
		template< unsigned CH >
		void TriggerSource( TriggeredFrom from ) noexcept
		{
			ChannelHelper<TriggeredHelper, CH> writter;
			writter( from );
		}
		/**
		 * @brief Select the trigger source for a DAC channel.
		 * 
		 * @tparam CH_1 The first channel index.
		 * @tparam CH_2 The second channel index.
		 * @param from The trigger source.
		 */
		template< unsigned CH_1, unsigned CH_2 >
		void TriggerSource( TriggeredFrom from ) noexcept
		{
			ChannelHelper<TriggeredHelper, CH_1, CH_2> writter;
			writter( from, from );
		}
		/**
		 * @brief Select the trigger source for two DAC channels.
		 * 
		 * @tparam CH_1 The first channel index. 
		 * @tparam CH_2 The second channel index.
		 * @param from_1 The trigger source of the first channel index.
		 * @param from_2 The trigger source of the second channel index.
		 */
		template< unsigned CH_1, unsigned CH_2 >
		void TriggerSource( TriggeredFrom from_1, TriggeredFrom from_2 ) noexcept
		{
			ChannelHelper<TriggeredHelper, CH_1, CH_2> writter;
			writter( from_1, from_2 );
		}
		/**
		 * @brief Triggers the requested DAC channels.
		 * 
		 * @tparam Channels Then channels to trigger.
		 */
		template< unsigned ... Channels >
		void Trigger() noexcept
		{
			auto const mask = General::MultiMaskBit<std::uint32_t>(Channels...);
			auto & trigger = DAC->SWTRIGR;
			trigger = mask;
		}
		/**
		 * @brief Helper for writing a value to the DAC.
		 */
		struct WriteHelper
		{
			using type = std::uint16_t;
			/**
			 * @brief Write an integer to the DAC1 output.
			 * 
			 * @param value The value to write to the register.
			 */
			static void Channel1(type value) noexcept
			{
				DAC->DHR12R1 = value;
			}
			/**
			 * @brief Write an integer to the DAC2 output.
			 * 
			 * @param value The value to write to the register.
			 */
			static void Channel2(type value) noexcept
			{
				DAC->DHR12R2 = value;
			}
		};
		/**
		 * @brief Writes values to two DAC channels.
		 * 
		 * @tparam CH_1 The index of the first channel to write.
		 * @tparam CH_2 The index of the second channel to write.
		 * @param value_1 The value to write into the first DAC output.
		 * @param value_2 The value to write into the second DAC output.
		 */
		template< std::uint32_t CH_1,  std::uint32_t CH_2 >
		void Write( std::uint32_t value_1, std::uint32_t value_2) noexcept
		{
			ChannelHelper<WriteHelper, CH_1, CH_2> writter;
			using t = typename WriteHelper::type;
			writter( (t)value_1, (t)value_2 );
		}
		/**
		 * @brief Writes a single value to both DAC outputs.
		 * 
		 * @tparam CH_1 The index of the first channel to write.
		 * @tparam CH_2 The index of the second channel to write.
		 * @param value The value to write into the DAC outputs.
		 */
		template< std::uint32_t CH_1,  std::uint32_t CH_2 >
		void Write( std::uint32_t value ) noexcept
		{
			ChannelHelper<WriteHelper, CH_1, CH_2> writter;
			writter( value, value );
		}
		/**
		 * @brief Writes a single value to a DAC output.
		 * 
		 * @tparam CH The index of the first channel to write.
		 * @param value The value to write into the DAC output.
		 */
		template<  std::uint32_t CH >
		void Write( std::uint32_t value ) noexcept
		{
			ChannelHelper<WriteHelper, CH> writter;
			writter( value );
		}
	}
	/**
	 * @brief Handles the enabling and disabling of the DAC, its buffers and the peripheral power.
	 * 
	 * @tparam Channels The channels to initialise. 
	 */
	template< std::uint32_t ... Channels>
	struct DACPowerKernal
	{
		//Port A, starts at bit 17
		static constexpr auto count		= sizeof...(Channels);
		static constexpr auto offset	= ( std::uint32_t )29u;
		static constexpr auto mask 		= ( std::uint32_t )1u << offset;
		/**
		 * @brief A representation of the DAC pins.
		 * 
		 * @tparam The DAC pin indexes.
		 */
		template< std::uint32_t ...>
		struct DACPins;
		/**
		 * @brief The representation of a single DAC pin.
		 * 
		 * @tparam CH The DAC channel that is being represented.
		 */
		template< std::uint32_t CH>
		class DACPins<CH>
		{
		private:
			using pin_t = typename ::General::AtIndex<CH - 1, DACGeneral::Pins_t>::type;
			pin_t m_Pin;
		public:
			DACPins() noexcept : m_Pin(IO::Mode::Analog) {}
		};
		/**
		 * @brief A representation of two DAC channels.
		 * 
		 * @tparam CH1 The index of the first DAC channel.
		 * @tparam CH2 The index of the second DAC channel.
		 */
		template< std::uint32_t CH1,  std::uint32_t CH2>
		class DACPins<CH1, CH2>
		{
			DACPins<CH1> m_Pin1;
			DACPins<CH2> m_Pin2;
		};
		/**
		 * @brief Should be called to enable the functionality of the DAC.
		 */
		static void Construct() noexcept
		{
			using namespace DACGeneral;
			volatile static DACPins<Channels...> pins;
			RCC->APB1ENR |= mask;

			Enabled< Channels ... >( true );
			Buffered< Channels ... >( false );
		}
		/**
		 * @brief Should be called to disable the functionality of the DAC.
		 */
		static void Destruct() noexcept
		{
			using namespace DACGeneral;
			Enabled< Channels ... >( false );
		    RCC->APB1ENR &= ~mask;
		}
	};
	/**
	 * @brief A representation of the DAC module.
	 * 
	 * @tparam Channels The channels used by the DAC.
	 */
	template< std::uint32_t ... Channels>
	class DACModule
	{
		using this_t 		= DACModule< Channels... >;
		using kernal_t 		= DACPowerKernal< Channels... >;
		using power_t 		= Power::Power<kernal_t>;

		static auto constexpr 	bits = 12u;
		static float constexpr 	max_value = 4095.f;

	private:
		power_t m_PSR;
		/**
		 * @brief Converts a voltage to a raw register value.
		 * 
		 * @param value The voltage to convert to a register value.
		 * @return std::uint16_t The output value.
		 */
		constexpr static std::uint16_t ToRegValue(float value) noexcept
		{
			return (std::uint16_t)(General::RoundedDivide(value * max_value, System::AnalogReference));
		}
	public:
		/**
		 * @brief Construct a new DACModule object
		 * 
		 * This handles calls the DACPowerKernal Construct function.
		 */
		DACModule() noexcept {}
		/**
		 * @brief Construct a new DACModule object
		 * 
		 * This handles calls the DACPowerKernal Destruct function.
		 */
		~DACModule() noexcept {}
		/**
		 * @brief Writes an integer too two DAC channels.
		 * 
		 * Written in the same order as the template parameter Channels.
		 * 
		 * @param value1 The value to write to the first DAC channel.
		 * @param value2 The value to write to the second DAC channel.
		 */
		static void Write( std::uint16_t value1, std::uint16_t value2) noexcept
		{
			DACGeneral::template Write< Channels... >( value1, value2 );
		}
		/**
		 * @brief Writes a value to a single DAC channel.
		 * 
		 * If thre are multiple channels, this writes the value to both.
		 * 
		 * @param value The value to write to the DAC channels.
		 */
		static void Write( std::uint16_t value ) noexcept
		{
			if constexpr (kernal_t::count == 1u)
				DACGeneral::template Write< Channels... >( value );

			if constexpr (kernal_t::count == 2u)
				DACGeneral::template Write< Channels... >( value, value );
		}
		/**
		 * @brief Writes a voltage to the DAC channel.
		 * 
		 * @note This is different to the Write functions, writing to them is a raw register write.
		 * @param value1 The voltage to write to the first DAC channel.
		 * @param value2 The voltage to write to the second DAC channel.
		 */
		static void SetVoltage(float value1, float value2) noexcept
		{
			Write( ToRegValue( value1 ), ToRegValue( value2 ) );
		}
		/**
		 * @brief Writes a voltage to the DAC channel.
		 * 
		 * @note If thre are multiple channels, this writes the value to both.
		 * @param value The voltage to write to the all DAC channel.
		 */
		static void SetVoltage(float value) noexcept
		{
			Write( ToRegValue( value) );
		}
	};
}h