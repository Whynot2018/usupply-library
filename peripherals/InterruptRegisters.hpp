#pragma once
#include "Math.hpp"
#include "Register.hpp"
#include "stm32f0xx.h"
#include <cstdint>

namespace System::InterruptGeneral
{
	namespace Helpers
	{
		template <unsigned N>
		constexpr bool ValidBit = (General::Between(N, 0u, 17u) || General::Between(N, 19u, 23u) || (N == 31u));
	}
	
	//Interrupt mask register (EXTI_IMR)
	template <unsigned N, std::size_t A = EXTI_BASE + offsetof(EXTI_TypeDef, IMR)>
	struct IMR : public General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		auto IM () noexcept
		{
			return base_t::template Actual<General::MaskBit(N)>();
		}
	};
	
	//Event mask register (EXTI_EMR)
	template <unsigned N, std::size_t A = EXTI_BASE + offsetof(EXTI_TypeDef, EMR)>
	struct EMR : public General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		auto EM () noexcept
		{
			return base_t::template Actual<General::MaskBit(N)>();
		}
	};
	
	//Rising trigger selection register (EXTI_RTSR)
	template <unsigned N>
	struct RTSR : public General::u32_reg<EXTI_BASE + offsetof(EXTI_TypeDef, RTSR)>
	{
		using base_t = General::u32_reg<EXTI_BASE + offsetof(EXTI_TypeDef, RTSR)>;
		auto RT() noexcept
		{
			static_assert(Helpers::ValidBit<N>, "This register doesn't support the selected bit.");
			return base_t::template Actual<General::MaskBit(N)>();
		}
	};
	
	//Falling trigger selection register (EXTI_FTSR)
	template <unsigned N>
	struct FTSR : public General::u32_reg<EXTI_BASE + offsetof(EXTI_TypeDef, FTSR)>
	{
		using base_t = General::u32_reg<EXTI_BASE + offsetof(EXTI_TypeDef, FTSR)>;		
		auto FT() noexcept
		{
			static_assert(Helpers::ValidBit<N>, "This register doesn't support the selected bit.");
			return base_t::template Actual<General::MaskBit(N)>();
		}
	};
	
	//Software interrupt event register (EXTI_SWIER)
	template <unsigned N>
	struct SWIER : public General::u32_reg<EXTI_BASE + offsetof(EXTI_TypeDef, SWIER)>
	{
		using base_t = General::u32_reg<EXTI_BASE + offsetof(EXTI_TypeDef, SWIER)>;
		
		auto SWI() noexcept
		{
			static_assert(Helpers::ValidBit<N>, "This register doesn't support the selected bit.");
			return base_t::template Actual<General::MaskBit(N)>();
		}
	};
	
	//Pending register (EXTI_PR)
	template <unsigned N, std::size_t A = EXTI_BASE + offsetof(EXTI_TypeDef, PR)>
	struct PR : public General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		
		auto PIF() noexcept
		{
			static_assert(Helpers::ValidBit<N>, "This register doesn't support the selected bit.");
			return base_t::template Actual<General::MaskBit(N)>();
		}
	};
	enum class InterruptEdge : std::uint32_t
	{
		None				= 0b00u,
		RisingEdge			= 0b01u,
		FallingEdge			= 0b10u,
		RisingFallingEdge	= 0b11u
	};
	enum class Interrupt : std::uint32_t
	{
		Enabled 	= 1,
		Disabled 	= 0
	};
	enum class Event : std::uint32_t
	{
		Enabled 	= 1,
		Disabled 	= 0
	};
	//
	template <unsigned N>
	struct EXTIChannel : IMR<N>, EMR<N>, RTSR<N>, FTSR<N>, SWIER<N>, PR<N>
	{
		/**
		 * These functions assign the interrupt functionality with
		 * no overhead (unless you work with assumptions of inital state).
		 */
		void Set(InterruptEdge input) noexcept
		{
			auto const value = General::UnderlyingValue(input);
			RTSR<N>::RT() = value & 1;	// Rising trigger selection
			FTSR<N>::FT() = value >> 1;	// Falling trigger selection
		}
		void Set(Event input) noexcept
		{
			EMR<N>::EM() = General::UnderlyingValue(input);
		}
		void Set(Interrupt input) noexcept
		{
			IMR<N>::IM() = General::UnderlyingValue(input);
		}
		template <typename ... Args>
		void Set(Args...args) noexcept
		{
			( Set( args ), ... );
		}
		/**
		 * These functions return the requested type
		 */
		template <typename T>
		T Get() const noexcept
		{
			using type = std::decay_t<T>;
			if constexpr (std::is_same_v<type, Interrupt>)
			{
				return (Interrupt)(IMR<N>::IM());
			}
			if constexpr (std::is_same_v<type, Event>)
			{
				return (Event)(EMR<N>::EM());
			}
			if constexpr (std::is_same_v<type, InterruptEdge>)
			{
				return (InterruptEdge)((FTSR<N>::FT() << 1u) | RTSR<N>::RT());
			}
		}
		/**
		 * Checks whether an interrupt is pending
		 */
		bool PendingFlag() noexcept
		{
			return PR<N>::PIF().Get();
		}
		void ClearFlag() noexcept
		{
			PR<N>::PIF() = 1u;
		}
		/**
		 * Software interrupt event register
		 */
		void Trigger() noexcept
		{
			SWIER<N>::SWI() = true;
		}
	};
}