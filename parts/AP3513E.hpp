#pragma once
#include "AD5260.hpp"

namespace Parts::AP3513E
{
	/**
	 * @brief A manager class for the AP3513E preregulator circuit on the usupply.
	 * 
	 * @tparam SPIKernal The underlying SPI kernal.
	 */
	template <typename SPIKernal>
	class AP3513E
	{
	private:
		/**
		 * @todo move this into Constants.hpp
		 */
		static constexpr float R_U    = 20000.0f;
		static constexpr float R_21   = 1500.0f;
		static constexpr float R_21_U = R_U + R_21;
		static constexpr float V_REF  = 0.925f;

		using POT_t = AD5260::AD5260<SPIKernal>;
		POT_t m_Potentiometer;
	public:
		/**
		 * @brief Construct a new AP3513E object
		 * 
		 * @tparam Args The types used to construct the underlying SPI object
		 * @param args The parameters required for the underlying SIP object.
		 */
		template <typename ... Args>
		AP3513E( Args && ... args ) noexcept :
			m_Potentiometer{ (unsigned)R_U, std::forward<Args>(args) ... }
		{}
		/**
		 * @brief Construct a new AP3513E object
		 * 
		 * @param input Initialises the AP3513E with a particular voltage.
		 * @tparam Args The types used to construct the underlying SPI object
		 * @param args The parameters required for the underlying SIP object.
		 */
		template <typename ... Args>
		AP3513E( float input, Args && ... args ) noexcept :
		    m_Potentiometer{ (unsigned)R_U, std::forward<Args>(args) ... }
		{
			*this = input;
		}
		/**
		 * @brief Sets the output voltage of the AP3513E circuit.
		 * 
		 * @tparam T The type of the input voltage.
		 * @param input The input voltage.
		 * @return AP3513E& For chained operations.
		 */
		template <typename T>
		AP3513E & operator=( T const& input ) noexcept
		{
			static_assert( std::is_floating_point_v<T>, "Must assign with a float or a double." );
			auto value = (T)1 - ( V_REF * R_21_U - input * R_21 ) / ( input * R_U );
			General::Ratio<T> ratio = ( value > 1.0f ) ? 1.0f : value;
			m_Potentiometer = ratio;
			return *this;
		}
	};
}