#pragma once

#include "Types.hpp"
#include "Meta.hpp"
#include <type_traits>

namespace General
{
	/**
	 * @brief A class to manage a matrix keypad.
	 * 
	 * @tparam Keypad_t The matrix keypad to sample.
	 * @tparam Buttons The buttons (potentially virtual multiplexed buttons with debouncing).
	 */
	template <typename Keypad_t, typename ... Buttons>
	class Keypad
	{
	private:
		Keypad_t m_MatrixKeypad;
		std::tuple<Buttons...> m_Data;
	public:
		/**
		 * @brief Construct a new Keypad object.
		 * 
		 * @param keypad The kernal (just used for type deduction of the class) of the keypad.
		 * @param args The different button constructors.
		 */
		Keypad(General::Type<Keypad_t> keypad, Buttons ... args) noexcept: 
			m_Data{ args ... }
		{}
		/**
		 * @brief Samples the matrix keypad and update all the buttons with the new matrix state.
		 */
		void Update() noexcept
		{
			//Go through all the buttons in the base class tuple, update them all (this causes callbacks to be called)
			General::ForEachTuple(m_Data, [](auto & e, unsigned index) noexcept
			{
				 e.Update();
			});
			//
			//Ensure the keypad is fully sampled.
			m_MatrixKeypad.Update();
		}
	};
	
	template <typename Keypad_t, typename ... Args>
	Keypad(General::Type<Keypad_t>,Args ... args) -> Keypad<Keypad_t, Args...>;
}