#pragma once
#include "IntegerConvert.hpp"
#include "ArrayConvert.hpp"
#include "Char.hpp"
#include "Math.hpp"

namespace General
{
	/**
	 * @brief Converts a floating point number and inserts it into an ascii char array.
	 * 
	 * @tparam N The capacity of the char array.
	 * @tparam T The floating point number type( double or float ).
	 * @tparam Decimals The number of decimal places to print.
	 * @tparam Base The base to use when printing the number.
	 * @param input The input floating point number
	 * @param output The output char array.
	 * @param offset The offset from the start of the output array to start printing. 
	 * @param padding Any blank cells that are printed will be printed with this character.
	 * @return std::size_t The number of characters inserted into the output array.
	 */
	template <std::size_t N, typename T = float, std::size_t Decimals = 4u, std::size_t Base = 10u>
	std::size_t ftoa( T input, std::array<char, N> &output, std::size_t const offset = 0u, char const padding = ' ' ) noexcept
	{
		auto constexpr Minimum = ( 1.0f / (float)General::Power<std::size_t, Base>( Decimals + 1 ) );
		input += Minimum * 5;

		// Returns the integer component then zeros it
		auto const withdraw = [] ( T & val )
		{
			auto const value = (long)val;
			val -= value;
			return value;
		};

		// Writes digits before decimal
		std::size_t Length = itoa<N, long>( withdraw( input ), output, offset, padding, ( input < (T)0 ) );
		input = std::abs( input );

		// Adds decimal point if it fits
		if ( Length < N ) output[ Length++ ] = '.';

		// Writes digits after decimal
		auto SigFigures{ Decimals };
		while ( ( ( input > (T)0 ) || ( SigFigures == Decimals ) ) && ( Length < N ) && ( 0u < ( SigFigures-- ) ) )
			output[ Length++ ] = FromNumber( (std::int8_t)withdraw( input *= (T)Base ) );

		//
		return Length;
	}
	/**
	 * @brief Converts a floating point number and inserts it into an ascii char FIFO.
	 * 
	 * @note This functions output is limited to the size of long. 
	 * This is done because full float implementations tend towards bloated.
	 * 
	 * @tparam N The capacity of the char FIFO.
	 * @tparam T The floating point number type( double or float ).
	 * @tparam Decimals The number of decimal places to print.
	 * @tparam Base The base to use when printing the number.
	 * @param input The input floating point number
	 * @param output The output char FIFO.
	 * @return std::size_t The number of characters inserted into the output FIFO.
	 */
	template <std::size_t N, typename T = float, std::size_t Decimals = 4u, std::size_t Base = 10u>
	std::size_t ftoa( T input, Containers::FIFO<char, N> & output ) noexcept
	{
		auto constexpr Minimum = ( 1.0f / (float)General::Power<std::size_t, Base>( Decimals + 1 ) );
		input += Minimum * 5;

		// Returns the integer component then zeros it
		auto const withdraw = []( T & val )
		{
			auto value = (long)val;
			val -= value;
			return value;
		};

		// Writes digits before decimal
		auto length = itoa<N, long>( withdraw( input ), output, ( input < (T)0 ) );
		if ( length == 0 ) return length;
		input = std::abs( input );

		// Adds decimal point if it fits
		if ( output.Push( '.' ) )
			++length;
		else
			return length;

		// Writes digits after decimal
		auto SigFig{Decimals};
		while ( ( ( (T)0 < input ) || ( SigFig == Decimals ) ) && ( 0u < ( SigFig-- ) ) )
		{
			if ( output.Push( FromNumber( withdraw( input *= (T)Base ) ) ) )
				++length;
			else 
				return length;
		}
		return length;
	}
	/**
	 * @brief Prints a floating point number into a character array, the resulting print will have a fixed length, padding is inserted.
	 * 
	 * @tparam fixed_length The number of characters to occupy with the output.
	 * @tparam N The capacity of the output array.
	 * @tparam T The type of the number to convert (float or double)
	 * @param input The number to convert.
	 * @param output The output array.
	 * @param offset The starting index to begin printing.
	 * @param padding Blank characters will be printed with this character. 
	 * @return std::size_t The number of characters inserted into the output array.
	 */
	template <std::size_t fixed_length, std::size_t N, typename T = float>
	constexpr std::size_t fixed_ftoa( T input, std::array<char, N> & output, std::size_t offset = 0, char padding = ' ' ) noexcept
	{
		static_assert( fixed_length <= N, "Cannot have write to an array smaller than result size." );
		using container = std::array<char, fixed_length>;
		std::size_t l = ftoa<fixed_length, T, fixed_length>( input, (container &)output, offset, padding );
		for ( auto i = l; i < fixed_length; ++i ) output[ i ] = '0';
		return fixed_length;
	}
	/**
	 * @brief Converts an ascii value into a floating point number.
	 * 
	 * This not support:
	 * - +inf
	 * - -inf
	 * - nan
	 * - Exponent notation 1.2E10 etc...
	 * 
	 * This is a barebones basic float convertion function.
	 * 
	 * @tparam N The capacity of the input array.
	 * @tparam T The type to store the result into.
	 * @param input The storage for the input array.
	 * @param output The result storage.
	 * @param offset The postiion in the input array to start parsing.
	 * @return std::size_t The number of characters inserted.
	 */
	template <std::size_t N, typename T>
	constexpr std::size_t atof( std::array<char, N> const & input, T & output, std::size_t offset = 0 ) noexcept
	{
		bool negative = false;
		T    before = (T)0, after = (T)0, division = (T)10;

		enum
		{
			Sign     = 0u,
			Number   = 1u,
			Mantissa = 2u,
			LetterF  = 3u
		} Current = Sign;

		std::size_t length = 0u;
		for ( auto i = offset; i < input.size(); ++i )
		{
			auto item = input[ i ];
			switch ( Current )
			{
			case Sign:
				if ( IsSign( item ) )
				{
					negative ^= ( item == '-' );
					break;
				}
				Current = Number;
				[[fallthrough]];
			case Number:
				if ( IsNumber( item ) )
				{
					auto value = ToNumber( item );
					before *= (T)10;
					before += value;
					break;
				}
				else if ( item != '.' )
				{
					output = before;
					if ( negative ) output = -output;
					return length;
				}
				Current = Mantissa;
				break;
			case Mantissa:
				if ( IsNumber( item ) )
				{
					auto value = ToNumber( item );
					after += (float)value / division;
					division *= (T)10;
					break;
				}
				Current = LetterF;
				[[fallthrough]];
			case LetterF:
				output = before + after;
				if ( negative ) output = -output;
				return length + ( item == 'f' );
			};
			++length;
		}
		if ( length > 0 )
		{
			output = before + after;
			if ( negative ) output = -output;
			return length;
		}
		return 0u;
	}
}