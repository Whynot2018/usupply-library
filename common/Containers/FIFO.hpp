#pragma once
#include "Macros.hpp"
#include <array>
#include <optional>

namespace Containers
{
	/**
	 * @brief A [ First In First Out - FIFO ] data structure.
	 * 
	 * @tparam T The type of the elements
	 * @tparam Length The capacity of the FIFO.
	 */
	template <typename T, std::size_t Length>
	class FIFO
	{
	protected:
		std::size_t				m_Start		= 0;
		std::size_t				m_Endpoint	= 0;
		std::array<T, Length> 	m_Buffer;

		constexpr void Wrap() noexcept
		{
			while ( m_Start >= Length && m_Endpoint >= Length )
			{
				m_Start 	-= Length;
				m_Endpoint 	-= Length;
			}
		}
		constexpr void IncrementStart() noexcept
		{
			++m_Start;
			Wrap();
		}
		constexpr void IncrementEnd() noexcept
		{
			++m_Endpoint;
			Wrap();
		}
		constexpr static std::size_t Index( std::size_t input ) noexcept
		{
			return input % Length;
		}
		constexpr std::size_t IndexStart() noexcept
		{
			return Index( m_Start );
		}
		constexpr std::size_t IndexEndpoint() noexcept
		{
			return Index( m_Endpoint );
		}
		constexpr T & Next() noexcept
		{
			return m_Buffer[ IndexEndpoint() ];
		}
		constexpr std::size_t SinglePush( T const & data ) noexcept
		{
			if ( Full() ) return 0u;
			Next() = data;
			IncrementEnd();
			return 1u;
		}

	public:
		using value_type = T;
		/**
		 * @brief Construct a new FIFO object
		 * 
		 * The construction of FIFO is always empty.
		 */
		constexpr FIFO() noexcept = default;
		/**
		 * @brief Inserts an arbitary number of items in the FIFO
		 * 
		 * @tparam Args The types of the items to insert.
		 * @param args The arguments to insert.
		 * @return constexpr std::size_t The number of successful insertions.
		 */
		template <typename ... Args>
		constexpr std::size_t Push( Args && ... args ) noexcept
		{
			return ( SinglePush( FWD( args ) ) + ... );
		}
		/**
		 * @brief The following determines when an item has been inserted in the FIFO.
		 * 
		 * @param data The data to compare with.
		 * @return true Item exists
		 * @return false Item doesn't exist
		 */
		constexpr bool Exists( T const & data ) noexcept
		{
			for ( auto const & item : ( *this ) )
				if ( item == data )
					return true;
			return false;
		}
		/**
		 * @brief Removes the item at the front of the FIFO
		 * 
		 * @return constexpr std::optional<T> returns nullopt when no item exists.
		 */
		constexpr std::optional<T> Pop() noexcept
		{
			if ( Empty() ) return {};
			T output = Front();
			IncrementStart();
			return output;
		}
		/**
		 * @brief Removes the front item from the array.
		 * 
		 * @return bool Returns true when pop is successful
		 */
		constexpr bool PopFront() noexcept
		{
			if ( Empty() )
			{
				return false;
			}
			else
			{
				IncrementStart();
				return true;
			}
		}
		/**
		 * @brief Removes all the items in the container
		 * 
		 */
		constexpr void Clear() noexcept
		{
			m_Start    = 0;
			m_Endpoint = 0;
		}
		/**
		 * @brief The index of the front item of the container
		 * 
		 * @return constexpr std::size_t 
		 */
		constexpr std::size_t FrontIndex() const noexcept
		{
			return Index(m_Start);
		}
		/**
		 * @brief The index of the back item of the container
		 * 
		 * @return constexpr std::size_t 
		 */
		constexpr std::size_t BackIndex() const noexcept
		{
			return Index(Index(m_Endpoint + Length - 1u));
		}
		/**
		 * @brief The front of the container
		 * 
		 * @return constexpr T & 
		 */
		constexpr T & Front() noexcept
		{
			return m_Buffer[ FrontIndex() ];
		}
		/**
		 * @brief A const version of the front of the container
		 * 
		 * @return constexpr T const& 
		 */
		constexpr T const & Front() const noexcept
		{
			return m_Buffer[ FrontIndex() ];
		}
		/**
		 * @brief The back of the container
		 * 
		 * @return constexpr T& 
		 */
		constexpr T & Back() noexcept
		{
			return m_Buffer[ BackIndex() ];
		}
		/**
		 * @brief A const version of the back of the container
		 * 
		 * @return constexpr T const& 
		 */
		constexpr T const & Back() const noexcept
		{
			return m_Buffer[ BackIndex() ];
		}
		/**
		 * @brief Get the number of items in the container.
		 * 
		 * @return constexpr std::size_t 
		 */
		constexpr std::size_t Size() const noexcept
		{
			return m_Endpoint - m_Start;
		}
		/**
		 * @brief Returns whether there is remaining space in the container.
		 * 
		 * @return constexpr std::size_t 
		 */
		constexpr std::size_t Space() const noexcept
		{
			return Length - Size();
		}
		/**
		 * @brief Gets the maximum number of items in the container.
		 * 
		 * @return constexpr std::size_t 
		 */
		constexpr std::size_t Capacity() const noexcept
		{
			return Length;
		}
		/**
		 * @brief Get whether or not (true or false), the container is empty.
		 * 
		 * @return true The container is empty.
		 * @return false The container is not empty.
		 */
		constexpr bool Empty() const noexcept
		{
			return Size() == 0;
		}
		/**
		 * @brief Gets whether or not the container is empty.
		 * 
		 * @return true The container is full.
		 * @return false The container is not full.
		 */
		constexpr bool Full() const noexcept
		{
			return Size() == Length;
		}
		/**
		 * @brief Gets a reference to the underlying buffer
		 * 
		 * NOTE: Data in a FIFO is NOT contigious.
		 * Manipulating pointers will not lead to expected behaviour.
		 * 
		 * @return T const& 
		 */
		T const & Data() const noexcept
		{
			return m_Buffer;
		}
		/**
		 * @brief Gets the item at an index in the container
		 * 
		 * @param index The index to get the item.
		 * @return T& The item.
		 */
		T & operator[]( std::size_t index ) noexcept
		{
			return m_Buffer[ Index( index + m_Start ) ];
		}
		/**
		 * @brief Gets the item at an index in the container.
		 * 
		 * @param index The item index.
		 * @return T const& The item.
		 */
		T const & operator[]( std::size_t const index ) const noexcept
		{
			return m_Buffer[ Index( index + m_Start ) ];
		}
	};
}