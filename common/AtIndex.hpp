#pragma once

namespace General
{
	/**
	 * @brief A list of types, used for metaprogramming.
	 * 
	 * @tparam args The types in the list.
	 */
	template <typename... args>
	struct TypeList;
	/**
	 * @brief The terminal case overload.
	 * 
	 * @tparam first The sole item in the type list.
	 */
	template <typename first>
	struct TypeList<first>
	{
		static constexpr auto count = 1;

		using head = first;
	};
	/**
	 * @brief The variadic overload.
	 * 
	 * @tparam first The first item in the list.
	 * @tparam args The arguments that are stored in the list.
	 */
	template <typename first, typename... args>
	struct TypeList<first, args...>
	{
		static constexpr auto count = 1 + sizeof...( args );

		using head = first;
	};
	/**
	 * @brief Returns the type at the index of a TypeList.
	 * 
	 * @tparam i The index in the type list.
	 * @tparam args The arguments in the type list.
	 */
	template <unsigned i, typename... args>
	struct AtIndex
	{
		using type = void;
	};
	/**
	 * @brief The single type overload.
	 * 
	 * @tparam head The sole item in the type list.
	 */
	template <typename head>
	struct AtIndex<0, TypeList<head>>
	{
		using type = head;
	};
	/**
	 * @brief The varidic overload
	 * 
	 * @tparam head The leading item in the type list. 
	 * @tparam args The types in the type list.
	 */
	template <typename head, typename... args>
	struct AtIndex<0, TypeList<head, args...>>
	{
		using type = head;
	};
	/**
	 * @brief This function is the intial overload, used to dispatch to the above specialisations of AtIndex.
	 * 
	 * This works by recursively calling AtIndex and on each recursion decrementing the index value.
	 * 
	 * @tparam i The index to return.
	 * @tparam head The first item in the type list.
	 * @tparam args The other items in the type list.
	 */
	template <unsigned i, typename head, typename... args>
	struct AtIndex<i, TypeList<head, args...>>
	{
		using type = typename AtIndex<i - 1, TypeList<args...>>::type;
	};
}