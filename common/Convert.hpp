#pragma once

#include "Char.hpp"
#include "ArrayConvert.hpp"
#include "BooleanConvert.hpp"
#include "FloatConvert.hpp"
#include "IntegerConvert.hpp"
#include "Boolean.hpp"
#include "Calibration.hpp"

namespace General
{
	/**
	 * @brief Skips all whitespace from the position onwards from index.
	 * 
	 * @tparam N The capacity of the input array.
	 * @param input The array to test for whitespaces.
	 * @param index The index to start testing.
	 * @return std::size_t The new index in the array (after the last contigious whitespace).
	 */
	template <std::size_t N>
	std::size_t Whitespace( std::array<char, N> const & input, std::size_t index = 0u ) noexcept
	{
		auto const start = index;
		while (index < N)
		{
			if (!IsWhitespace(input[index])) break;
			++index;
		}
		return index - start;
	}
	/**
	 * @brief Convert an input into a string and insert it into the output variable.
	 * 
	 * @tparam N The capacity of the output array.
	 * @tparam T The type of the input.
	 * @param input The input value.
	 * @param output The output array, the result is stored here.
	 * @param offset The starting index, the first character converted is stored in this index.
	 * @param padding The padding to insert if spaces is produced.
	 * @return std::size_t The number of characters inserted into the array.
	 */
	template <std::size_t N, typename T>
	std::size_t xtoa( T input, std::array<char, N> & output, std::size_t offset = 0u, char padding = ' ' ) noexcept
	{
		using base_t = typename std::decay_t<T>;

		if constexpr ( std::is_floating_point_v<base_t> )
			return ftoa<N, T>( input, output, offset, padding );

		if constexpr ( std::is_unsigned_v<T> )
			return utoa<N, T>( input, output, offset, padding );

		if constexpr ( std::is_signed_v<T> )
			return itoa<N, T>( input, output, offset, padding );

		if constexpr ( General::IsArray<T>::value )
			return atoa<N>( input, output, offset, padding );

		if constexpr ( General::is_boolean_v<base_t> )
		{
			return ( (bool)input ) ? 
				atoa<N>( input.true_str, output, offset, padding ):
				atoa<N>( input.false_str, output, offset, padding );
		}

		if constexpr ( General::is_sample_v<base_t> )
			return input.ToString( output, offset );

		return 0u;
	}
	/**
	 * @brief Converts the input to a string and inserts it into the FIFO.
	 * 
	 * @tparam N The capacity of the output FIFO.
	 * @tparam T The type of the input.
	 * @param input The input value.
	 * @param output The FIFO to insert the result.
	 * @return std::size_t The number of characters inserted into the FIFO.
	 */
	template <std::size_t N, typename T>
	std::size_t xtoa( T input, Containers::FIFO<char, N>& output ) noexcept
	{
		using base_t = typename std::decay_t<T>;

		if constexpr ( std::is_floating_point_v<base_t> )
			return ftoa<N, T>( input, output );

		if constexpr ( std::is_unsigned_v<T> )
			return utoa<N, T>( input, output );

		if constexpr ( std::is_signed_v<T> )
			return itoa<N, T>( input, output );

		if constexpr ( General::IsArray<T>::value )
			return atoa<N>( MakeArray( input ), output );

		if constexpr ( std::is_same_v<char, std::decay_t<T>> )
			return ctoa<N>( input, output );

		if constexpr ( General::is_boolean_v<std::decay_t<T>> )
		{
			return ( (bool)input ) ? 
				atoa<N>( input.true_str, output ):
				atoa<N>( input.false_str, output );
		}
		if constexpr ( General::is_sample_v<base_t> )
			return input.ToString( output );

		return 0u;
	}
	/**
	 * @brief Inserts a C array into a fifo.
	 * 
	 * @tparam N The capacity of the output FIFO.
	 * @tparam T The type of the input. This should be char.
	 * @tparam L The capacity of the input array.
	 * @param input The input array.
	 * @param output The output FIFO.
	 * @return std::size_t The number of chacaters inserted into the output FIFO.
	 */
	template <std::size_t N, std::size_t L>
	std::size_t xtoa( CArray<char, L> const& input, Containers::FIFO<char, N>& output )
	{
		return atoa( MakeArray( input ), output );
	}
	/**
	 * @brief Converts a container to value.
	 * 
	 * @tparam container The container that will be read.
	 * @tparam std::size_t The capacity of container.
	 */
	template <typename container, typename T = std::size_t>
	struct fixed;
	template <template <typename, std::size_t> class container, std::size_t N, typename T>
	struct fixed<container<char, N>, T>
	{
		using container_t = container<char, N>;
		/**
		 * @brief Reads from a container and returns the resultant value.
		 * 
		 * @param input the container to read from.
		 * @return auto const The result of the conversion from the container to the value.
		 */
		auto const operator()( container_t& input ) noexcept
		{
			using iterator  = typename container_t::iterator;
			std::size_t length = 0u;
			auto uint = [&]( iterator& start, iterator& end )
			{
                T output = (T)0;
                for ( ; start != end; ++start )
                {
                    auto const& item = *start;
                    if ( IsNumber( item ) )
                    {
                        uint8_t value = ToNumber( item );
						//
                        output *= (T)10;
                        output += value;
						//
                        ++length;
                    }
                    else break;
                }
                return output;
			};
			auto sint = [&]( iterator& start, iterator& end ) {
				bool negative = false;
				//
				while ( true )
				{
					auto const front = *start;
					if ( front == '-' )
					{
						negative = !negative;
						++start;
					}
					else if ( front == '+' )
					{
						++start;
					}
					else
					{
						break;
					}
				}
				//
				T const uout = (T)uint( start, end );
				return ( negative ) ? -uout : uout;
			};
			//
			iterator begin = input.begin();
			iterator end   = input.end();
			//
			if constexpr ( std::is_unsigned_v<T> )
				return uint( begin, end );
			//
			if constexpr ( std::is_integral_v<T> )
				return sint( begin, end );
			//
			if constexpr ( std::is_floating_point_v<T> )
			{
				T output = sint( begin, end );
				//
				if ( *begin == '.' && begin != end )
					++begin;
				//
				auto const eg = *begin;
				//
				length = 0u;
				T decm = uint( begin, end );
				//
				while ( length-- )
					decm /= (T)10;
				//
				return output + decm;
			}
		}
	};
}