#pragma once
#include <optional>
#include <cstdint>

namespace Parser
{
	/**
	 * @brief The result of a parse operation.
	 */
	class ParseResult
	{
	private:
		std::size_t	const m_Length	= 0;
		bool		const m_Valid	= false;
	public:
		/**
		 * @brief Default construction of a ParseResult is equivilent to an invalid parse result.
		 */
		constexpr ParseResult() = default;
		/**
		 * @brief Store a result based on a boolean
		 */
		constexpr ParseResult( bool valid ) noexcept : 
			m_Length(0),
			m_Valid(valid) 
		{}
		/**
		 * @brief Store a valid result and a length.
		 */
		constexpr ParseResult( std::size_t length ) noexcept :
			m_Length( length ),	
			m_Valid( true )
		{}
		/**
		 * @brief Copy a parse result.
		 */
		constexpr ParseResult(ParseResult const & input) noexcept : 
			m_Length(input.m_Length), 
			m_Valid(input.m_Valid) 
		{}
		/**
		 * @brief No copy assignment as this should never be the usage pattern.
		 */
		constexpr ParseResult & operator = (ParseResult const &) = delete;
		/**
		 * @brief Check if the ParseResult is a valid result.
		 * 
		 * @return true Parse result is valid.
		 * @return false Parse result is invalid.
		 */
		constexpr operator bool() const noexcept
		{
			return m_Valid;
		}
		/**
		 * @brief Get the length of the parse result (number of characters parsed usually)
		 * 
		 * @return size_t The length to return.
		 */
		constexpr size_t Length() const noexcept
		{
			return m_Length;
		}
		/**
		 * @brief Return a parse result with the length incremented.
		 * 
		 * @return constexpr ParseResult 
		 */
		constexpr ParseResult AddLength() const noexcept
		{
			return { m_Length + 1 };
		}
		using Result_t = void;
	};
	/**
	 * @brief Stores the result of a parse operation and the number of characters consumed by the operation.
	 * 
	 * @tparam T The type of the value.
	 */
	template <typename T>
	class ValueParseResult
	{
	private:
		std::optional<T>	const m_Value	= {};
		std::size_t			const m_Length	= 0;
	public:
		/**
		 * @brief Default construction of a ParseResult is equivilent to an invalid parse result.
		 */
		constexpr ValueParseResult() = default;
		/**
		 * @brief Store a valid value and length.
		 * 
		 * The length here could be zero, that could be valid.
		 * 
		 * @param value The value to store.
		 * @param length The number of characters consumed.
		 */
		constexpr ValueParseResult( T const & value, std::size_t length ) noexcept :
			m_Value	( value ),
			m_Length( length ) 
		{}
		/**
		 * @brief Store a valid value and length.
		 * 
		 * The length here could be zero, that could be valid.
		 * 
		 * @param value The value to store.
		 * @param length The number of characters consumed.
		 */
		constexpr ValueParseResult( T && value, std::size_t length ) noexcept :
			m_Value	( std::forward<T>(value) ),
			m_Length( length )
		{}
		/**
		 * @brief Check if the ParseResult is a valid result.
		 * 
		 * @return true Parse result is valid.
		 * @return false Parse result is invalid.
		 */
		constexpr operator bool() const noexcept 
		{
			return m_Value.has_value();
		}
		/**
		 * @brief Get the length of the parse result (number of characters parsed usually)
		 * 
		 * @return size_t The length to return.
		 */
		constexpr std::size_t Length() const noexcept
		{
			return m_Length;
		}
		/**
		 * @brief Return a parse result with the length incremented.
		 * 
		 * @return constexpr ParseResult 
		 */
		constexpr ValueParseResult AddLength() const noexcept
		{
			return { m_Value.value(), m_Length + 1 };
		}
		/**
		 * @brief Get the stored value in the class
		 * 
		 * @warning Do not call if the class is not valid. 
		 * @return constexpr T const& 
		 */
		constexpr T const & Value() const noexcept
		{
			return m_Value.value();
		}
		/**
		 * @brief Add some more information to the parse result.
		 * 
		 * @tparam Tnew The type of the new information to store.
		 * @param newinfo The new information.
		 * @return ValueParseResult<std::pair<Tnew, T>> A new ValueParseResult with a pair of the current information and the old information. 
		 */
		template <typename Tnew>
		constexpr auto AddInformation(Tnew && newinfo) noexcept -> ValueParseResult<std::pair<Tnew, T>>
		{
			return { std::make_pair(std::move(newinfo), m_Value.value()), m_Length };
		}
		//
		template <typename Tnew>
		using AddInformation_t = ValueParseResult<std::pair<Tnew, T>>;
		using Result_t = T;
	};
	
	template <typename T>
	ValueParseResult(T, std::size_t)->ValueParseResult<T>;
	/**
	 * @brief Checks whether a type is a ValueParseResult type.
	 * 
	 * @tparam T The type to check.
	 */
	template <typename T>
	bool constexpr IsValueParseResult_v = false;
	template<typename T>
	bool constexpr IsValueParseResult_v < ValueParseResult<T> > = true;
}