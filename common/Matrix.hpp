#pragma once

#include "AtIndex.hpp"
#include "VirtualPort.hpp"
#include <type_traits>

namespace IO
{
	/**
	 * @brief A class which represents a keypad matrix.
	 * 
	 * @tparam Rows A list of all the pins in the row.
	 * @tparam Columns A list of all the pins in a column.
	 */
	template <typename Rows, typename Columns>
	class Matrix;
	/**
	 * @brief A class which represents a keypad matrix.
	 * 
	 * @tparam row_pins A list of all the pins in the row.
	 * @tparam column_pins A list of all the pins in a column.
	 */
	template <typename... row_pins, typename... column_pins>
	class Matrix<General::TypeList<row_pins...>, General::TypeList<column_pins...>> : row_pins..., column_pins...
	{
	public:
		using row_list_t    = General::TypeList<row_pins...>;
		using column_list_t = General::TypeList<column_pins...>;
		using unique_key_t  = General::TypeList<row_pins..., column_pins...>;

		static constexpr auto RowCount   = sizeof...( row_pins );
		static constexpr auto RowIndexes = std::make_index_sequence<RowCount>{};

		static constexpr auto ColumnCount   = sizeof...( column_pins );
		static constexpr auto ColumnIndexes = std::make_index_sequence<ColumnCount>{};

		using port_t = VirtualPort<unsigned, unique_key_t>;
		template <std::size_t row, std::size_t column>
		using pin_t = typename port_t::template Pin_t<row * ColumnCount + column>;

	private:
		static constexpr auto PinCount = RowCount + ColumnCount;
		static_assert( PinCount <= ( sizeof( unsigned ) * 8 ), "Cannot have matrix with more pins than can be stored in a VirtualPort (bits in unsigned)." );

		port_t m_Matrix;

		template <unsigned index>
		auto& ColumnsPin() noexcept
		{
			return (typename General::AtIndex<index, column_list_t>::type&)( *this );
		}
		template <unsigned index>
		auto& RowsPin() noexcept
		{
			return (typename General::AtIndex<index, row_list_t>::type&)( *this );
		}
		template <std::size_t... Is>
		unsigned ReadColumns( std::index_sequence<Is...> c ) noexcept
		{
			return ( ( ( (unsigned)(bool)ColumnsPin<Is>() ) << Is ) | ... );
		}
		template <std::size_t Setting, std::size_t... Is>
		void SetRows( std::index_sequence<Is...> c ) noexcept
		{
			auto constexpr setting = 1 << Setting;
			( ( RowsPin<Is>() = ( (bool)( ( setting >> Is ) & 1 ) ) ), ... );
		}
		template <std::size_t... Is>
		void ClearRows( std::index_sequence<Is...> c ) noexcept
		{
			( ( RowsPin<Is>() = false ), ... );
		}
		template <std::size_t Row, size_t... Columns>
		void DoColumn( std::index_sequence<Columns...> ) noexcept
		{
			SetRows<Row>( RowIndexes );

			auto const column{ReadColumns( ColumnIndexes )};
			( ( ( pin_t<Row, Columns>() ) = (bool)( ( column >> Columns ) & 1 ) ), ... );

			ClearRows( RowIndexes );
		}
		template <size_t... Rows>
		auto PrivateUpdate( std::index_sequence<Rows...> ) noexcept
		{
			( DoColumn<Rows>( ColumnIndexes ), ... );
		}

	public:
		/**
		 * @brief Construct a new Matrix object
		 */
		Matrix() noexcept:
		    row_pins( Mode::Output, State::Low )...,
		    column_pins( Mode::Input, State::Low, Type::PushPull, Speed::High, Resistors::PullDown )...
		{}
		/**
		 * @brief Samples each of the pins and updates the corresponding virtual pin values.
		 */
		void Update() noexcept
		{
			//At compile time it generates items needed for the fold expression
			PrivateUpdate( RowIndexes );
		}
	};
}